﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using System.Web.Http.Description;
using apisiscracha.Contexto;
using apisiscracha.Models;

namespace apisiscracha.Controllers
{
    public class Funapi2Controller : ApiController
    {
        private DBContext db = new DBContext();

        // GET: api/Funapi2
        public IQueryable<Funcionarios> GetFuncionarios()
        {
            return db.Funcionarios;
        }
        [HttpGet]
        public IHttpActionResult Teste()
        {
            var stream = new MemoryStream();

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.GetBuffer())
            };
            result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "test.txt"
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            var response = ResponseMessage(result);

            return response;
        }


        [HttpGet]
        public IHttpActionResult Generate([FromUri] char id)
        {
            string dadostext = null;

            foreach (var func in GetFuncionarios())
            {
                if (id == 'C')
                {
                    dadostext += func.Id + ";" + func.Matricula + ";" + func.Cracha.Substring(2, func.Cracha.Length - 2) + ";\n";
                }
                else
                { //converte os 6 ultimos digitos do hexadecimal
                    dadostext += func.Id + ";" + func.Matricula + ";" + int.Parse(func.Cracha.Substring(6, func.Cracha.Length - 6), System.Globalization.NumberStyles.HexNumber).ToString() + ";\n";
                }
            }

            //string dadostext = JsonConvert.SerializeObject(dados, Formatting.Indented, new JsonSerializerSettings { Formatting = Formatting.Indented, TypeNameHandling = TypeNameHandling.Auto });
            byte[] textAsBytes = Encoding.Unicode.GetBytes(dadostext);


            var stream = new MemoryStream(textAsBytes);
            // processing the stream.

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.ToArray())
            };
            result.Content.Headers.ContentDisposition =
                new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = "teste.csv"
                };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/octet-stream");

            var response = ResponseMessage(result);
            return response;
        }


        // GET: api/Funapi2/5
        [ResponseType(typeof(Funcionarios))]
        public IHttpActionResult GetFuncionarios(int id)
        {
            Funcionarios funcionarios = db.Funcionarios.Find(id);
            if (funcionarios == null)
            {
                return NotFound();
            }

            return Ok(funcionarios);
        }

        // PUT: api/Funapi2/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFuncionarios(int id, Funcionarios funcionarios)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != funcionarios.Id)
            {
                return BadRequest();
            }

            db.Entry(funcionarios).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FuncionariosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;


                }
            }



            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Funapi2
        [ResponseType(typeof(Funcionarios))]
        public IHttpActionResult PostFuncionarios(Funcionarios funcionarios)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Funcionarios.Add(funcionarios);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = funcionarios.Id }, funcionarios);
        }

     

        // DELETE: api/Funapi2/5
        [ResponseType(typeof(Funcionarios))]
        public IHttpActionResult DeleteFuncionarios(int id)
        {
            Funcionarios funcionarios = db.Funcionarios.Find(id);
            if (funcionarios == null)
            {
                return NotFound();
            }

            db.Funcionarios.Remove(funcionarios);
            db.SaveChanges();

            return Ok(funcionarios);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FuncionariosExists(int id)
        {
            return db.Funcionarios.Count(e => e.Id == id) > 0;
        }
    }
}