﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using apisiscracha.Contexto;
using apisiscracha.Models;

namespace apisiscracha.Controllers
{
    public class FapiController : ApiController
    {
        private DBContext db = new DBContext();

        // GET: api/Fapi
        public IQueryable<Funcionarios> GetFuncionarios()
        {
            return db.Funcionarios;
        }
        [HttpGet]
        [Route("DefaultApi")]
        public IHttpActionResult GetFile(string fileName)
        {
            //Create HTTP Response.
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            //Set the File Path.
            string filePath = HttpContext.Current.Server.MapPath("~/Files/") + fileName;

            //Check whether File exists.
            if (!File.Exists(filePath))
            {
                //Throw 404 (Not Found) exception if File not found.
                response.StatusCode = HttpStatusCode.NotFound;
                response.ReasonPhrase = string.Format("File not found: {0} .", fileName);
                throw new HttpResponseException(response);
            }

            //Read the File into a Byte Array.
            byte[] bytes = File.ReadAllBytes(filePath);

            //Set the Response Content.
            response.Content = new ByteArrayContent(bytes);

            //Set the Response Content Length.
            response.Content.Headers.ContentLength = bytes.LongLength;

            //Set the Content Disposition Header Value and FileName.
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = fileName;

            //Set the File Content Type.
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(fileName));
            return (IHttpActionResult)response;
        }
        // GET: api/Fapi/5
        [ResponseType(typeof(Funcionarios))]
        public IHttpActionResult GetFuncionarios(int id)
        {
            Funcionarios funcionarios = db.Funcionarios.Find(id);
            if (funcionarios == null)
            {
                return NotFound();
            }

            return Ok(funcionarios);
        }


        [HttpGet]

        public IHttpActionResult GetFileAsync(int fileId)
        {
            // NOTE: If there was any other 'async' stuff here, then you would need to return
            // a Task<IHttpActionResult>, but for this simple case you need not.

            return new FileActionResult(fileId);
        }

        public class FileActionResult : IHttpActionResult
        {
            public FileActionResult(int fileId)
            {
                this.FileId = fileId;
            }

            public int FileId { get; private set; }

            public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                HttpResponseMessage response = new HttpResponseMessage();
                response.Content = new StreamContent(File.OpenRead(@"<Data source=PR661ET0032\SQLEXPRESS;initial catalog=siscracha;user id=root_pl;password=Abc@123>" + FileId));
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");

                // NOTE: Here I am just setting the result on the Task and not really doing any async stuff. 
                // But let's say you do stuff like contacting a File hosting service to get the file, then you would do 'async' stuff here.

                return Task.FromResult(response);
            }
        }

        // PUT: api/Fapi/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFuncionarios(int id, Funcionarios funcionarios)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != funcionarios.Id)
            {
                return BadRequest();
            }

            db.Entry(funcionarios).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FuncionariosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Fapi
        [ResponseType(typeof(Funcionarios))]
        public IHttpActionResult PostFuncionarios(Funcionarios funcionarios)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Funcionarios.Add(funcionarios);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = funcionarios.Id }, funcionarios);
        }

        // DELETE: api/Fapi/5
        [ResponseType(typeof(Funcionarios))]
        public IHttpActionResult DeleteFuncionarios(int id)
        {
            Funcionarios funcionarios = db.Funcionarios.Find(id);
            if (funcionarios == null)
            {
                return NotFound();
            }

            db.Funcionarios.Remove(funcionarios);
            db.SaveChanges();

            return Ok(funcionarios);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FuncionariosExists(int id)
        {
            return db.Funcionarios.Count(e => e.Id == id) > 0;
        }
    }
}