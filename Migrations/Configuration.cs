namespace apisiscracha.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using apisiscracha.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<apisiscracha.Contexto.DBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(apisiscracha.Contexto.DBContext context)
        {
            context.Funcionarios.AddOrUpdate(x => x.Id);
      

        }
    }
}
